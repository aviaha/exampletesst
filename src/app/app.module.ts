import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { ItemsComponent } from './items/items.component';
import { ItemComponent } from './item/item.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { Routes, RouterModule } from '@angular/router';


import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatCheckboxModule} from '@angular/material/checkbox';


import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import {environment} from '../environments/environment';
import { FormsModule } from '@angular/forms';
import { GreetingsComponent } from './greetings/greetings.component';


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    ItemsComponent,
    ItemComponent,
    LoginComponent,
    NotFoundComponent,
    GreetingsComponent
  ],
  imports: [
    BrowserModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    BrowserAnimationsModule,
    MatCheckboxModule,
  AngularFireModule.initializeApp(environment.firebase),
  AngularFireDatabaseModule,
  AngularFireAuthModule,
  FormsModule,
    RouterModule.forRoot([

     {path:'',component:ItemsComponent}, //היו אר אל ריק כי אלה דפים משתנים
    {path:'items',component:ItemsComponent},
      {path:'login',component:LoginComponent},
    {path:'greetings',component:GreetingsComponent},
      //{path:'usertodos',component:UsertodosComponent},
      {path:'**',component:NotFoundComponent}//לא לשים אחריו את ראוט,זה לא יעבוד
     
     
    ])
 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
