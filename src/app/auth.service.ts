import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  login(email: string, password: string) {
    return this.fireBaseAuth.auth.signInWithEmailAndPassword(email, password);
  }
  logOut() {
    return this.fireBaseAuth.auth.signOut();
  }
 


  user: Observable<firebase.User>;
 constructor(private fireBaseAuth:AngularFireAuth) { 
  this.user = fireBaseAuth.authState;
 }

}
